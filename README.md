# Memory

A simple memory game realized with [Alpine.js](https://alpinejs.dev), [Open Props](https://www.open-props.style) and [Phosphor Icons](https://phosphoricons.com). Animations are all realized with CSS only.

## Styles

Styles are implemented as objects with the following attributes:

- `name`: The name of the style.
- `template`: The HTML content, that is added to the cards front `card_content` is replaced with the content from the `content` array.
- `content`: The array with all card contents.
- `back`: Text on the back of the cards.
- `back_style`: The style of the back face of the cards. Can be any type of CSS to create a background.
- `init`: A callback function, which is applied, when the style is initialized.

The user can select one of the styles before starting a game.

### Example styles

There are 3 example styles available

#### Random Image

Card faces are random images from [Lorem Picsum](https://picsum.photos). `content` is just an array of integers, which are used as seed to fetch the images.

#### Flowers

The same concept as before, but card faces are random images of flowers from [LoremFlickr](https://loremflickr.com).

#### Amino acid

Uses [SMILES](https://en.wikipedia.org/wiki/Simplified_molecular-input_line-entry_system) strings to draw chemical structures of of amino acids using [SmilesDrawer](https://smilesdrawer.surge.sh/). The SmilesDrawer script is added and invoked in the `init()` callback function.

# License

MIT license
