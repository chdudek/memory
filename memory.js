const STYLES = [
  {
    name: "Random Image",
    template: `<img src="https://picsum.photos/seed/${Date.now()}-card-{card_content}/200/200" />`,
    content: [...Array(16).keys()],
    back: "",
    back_style: `background-image:
    radial-gradient(closest-side, transparent 0%, transparent 75%, #B6CC66 76%, #B6CC66 85%, #EDFFDB 86%, #EDFFDB 94%, #FFFFFF 95%, #FFFFFF 103%, #D9E6A7 104%, #D9E6A7 112%, #798B3C 113%, #798B3C 121%, #FFFFFF 122%, #FFFFFF 130%, #E0EAD7 131%, #E0EAD7 140%),
    radial-gradient(closest-side, transparent 0%, transparent 75%, #B6CC66 76%, #B6CC66 85%, #EDFFDB 86%, #EDFFDB 94%, #FFFFFF 95%, #FFFFFF 103%, #D9E6A7 104%, #D9E6A7 112%, #798B3C 113%, #798B3C 121%, #FFFFFF 122%, #FFFFFF 130%, #E0EAD7 131%, #E0EAD7 140%);
    background-size: 30px 30px;
    background-color: #C8D3A7;
    background-position: 0 0, 15px 15px;
    outline: 1px dashed #B6CC66; outline-offset: -.5vw;`,
  },
  {
    name: "Flowers",
    template: `<img src="https://loremflickr.com/200/200/flower?random={card_content}" />`,
    content: [...Array(16).keys()],
    back: "Flowers",
    back_style: `background-color:white;
    background-image:
    radial-gradient(var(--indigo-4) 4px, transparent 5px),
    repeating-radial-gradient(var(--indigo-4) 0, var(--indigo-4) 2px, transparent 3px, transparent 10px, var(--indigo-4) 11px, var(--indigo-4) 12px, transparent 13px, transparent 25px);
    background-size: 15px 15px, 45px 45px; color: #ccc;-webkit-text-fill-color: var(--indigo-8); -webkit-text-stroke: 4px white; font-weight: bold; paint-order: stroke;
    background-position: 0 0; outline: 1px solid var(--indigo-4); outline-offset: -.5vw;`,
  },
  {
    name: "Amino acid",
    template: '<img data-smiles="{card_content}" />',
    content: [
      "NC(C)C(O)=O",
      "NC(CCCNC(N)=N)C(O)=O",
      "NC(CC(N)=O)C(O)=O",
      "NC(CC(O)=O)C(O)=O",
      "NC(CS)C(O)=O",
      "NC(CCC(N)=O)C(O)=O",
      "NC(CCC(O)=O)C(O)=O",
      "NC([H])C(O)=O",
      "NC(Cc1ccncn1)C(O)=O",
      "NC(C(CC)C)C(O)=O",
      "NC(CC(C)C)C(O)=O",
      "NC(CCCCN)C(O)=O",
      "NC(CCSC)C(O)=O",
      "NC(Cc1ccccc1)C(O)=O",
      "OC(C1CCCN1)=O",
      "NC(CO)C(O)=O",
      "NC(C(C)O)C(O)=O",
      "NC(Cc1cnc2c1cccc2)C(O)=O",
      "NC(Cc1ccc(O)cc1)C(O)=O",
      "NC(C(C)C)C(O)=O",
    ],
    init: function () {
      const script = document.createElement("script")
      script.setAttribute(
        "src",
        "https://unpkg.com/smiles-drawer@2.1.5/dist/smiles-drawer.min.js"
      )
      document.head.appendChild(script)
      script.onload = () => SmiDrawer.apply()
    },
    back_style: `background-color: #026873;
    background-image: linear-gradient(90deg, rgba(255,255,255,.07) 50%, transparent 50%),
    linear-gradient(90deg, rgba(255,255,255,.13) 50%, transparent 50%),
    linear-gradient(90deg, transparent 50%, rgba(255,255,255,.17) 50%),
    linear-gradient(90deg, transparent 50%, rgba(255,255,255,.19) 50%);
    background-size: 13px, 29px, 37px, 53px;
    outline: 1px dashed #ffffff; outline-offset: -.5vw;`,
    back: "Amino acids",
  },
]

/**
 * Game function that exposes the actual game to AlpineJS
 * @return {object} The game object AlpineJS uses
 */
function Memory() {
  return {
    /** Indicates if the user can turn or not */
    canTurn: false,
    /** The state if the game was initialized or not */
    init: false,
    /** The state if the game is running or not */
    running: false,
    /** Status of the game (-1: no pair found, 1: pair found, 0: waiting for selection) */
    status: 0,
    /** Number of used turns */
    turns: 0,
    /** The id of the selected card style */
    style_id: 0,
    /** The internal number of pairs */
    _num_pairs: 5,
    /** An array of all memory cards */
    cards: [],
    /** An array of currently flipped memory cards */
    cards_flipped: [],
    /** An array of all found memory cards */
    cards_found: [],
    /**
     * Gets the number of rows and columns of the memory field
     * @return {number}
     */
    get game_size() {
      return Math.ceil(Math.sqrt(this.num_pairs * 2))
    },
    /**
     * Gets the numbers of cards
     * @return {number}
     */
    get num_cards() {
      return this.num_pairs * 2
    },
    /**
     * Sets the numbers of pairs to use based on maximum number of pairs from the chosen style
     * @param {number} n The number of pairs to use
     */
    set num_pairs(n) {
      this._num_pairs = Math.min(n, this.style.content.length)
    },
    /**
     * Gets the number of pairs clamped at the maximum number of pairs from the chosen style
     * @return {number}
     */
    get num_pairs() {
      this._num_pairs = Math.min(this._num_pairs, this.style.content.length)
      return this._num_pairs
    },
    /**
     * Gets the style object by currently selected style id
     * @return {object} The style object
     */
    get style() {
      return STYLES[this.style_id]
    },
    /**
     * Starts a new game
     */
    start_game: function () {
      this.init = false
      this.running = true
      this.turns = 0
      this.cards_flipped = []
      this.cards_found = []
      this.cards = [...Array(this.num_cards / 2).keys()]
        .concat([...Array(this.num_cards / 2).keys()])
        .sort(() => 0.5 - Math.random())
    },
    /**
     * Combines card template and content of the given style
     * @param {number} card_id The id of the card
     * @returns {string} The parsed content of the card front
     */
    get_image: function (card_id) {
      let content = this.style.content[card_id]
      return this.style.template.replace("{card_content}", content)
    },
    /**
     * Handles flipping of cards
     * @param {*} card_id
     * @returns
     */
    flip_card: function (card_id) {
      if (!this.init && this.style.init !== undefined) {
        this.style.init()
      }
      if (this.canTurn) return
      if (this.cards_found.includes(card_id)) return

      if (!this.cards_flipped.includes(card_id)) {
        this.cards_flipped.push(card_id)
      }

      if (this.cards_flipped.length == 2) {
        this.turns += 1
        let c1 = this.cards_flipped[0]
        let c2 = this.cards_flipped[1]
        if (this.cards[c1] === this.cards[c2]) {
          this.canTurn = true
          this.status = 1
          setTimeout(() => {
            this.cards_found.push(c1)
            this.cards_found.push(c2)
            this.canTurn = false
            this.status = 0
            this.cards_flipped = []
          }, 2500)
          return
        }
        this.canTurn = true
        this.status = -1
        setTimeout(() => {
          this.cards_flipped = []
          this.canTurn = false
          this.status = 0
        }, 3000)
      }
    },
  }
}
